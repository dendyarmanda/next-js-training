import Proptypes from "prop-types";

/**
 *
 * @param {Object} props
 * @param {String} props.name
 * @param {(number|null|undefined)} props.age //kalo kurang yakin type datanya apaan
 * @returns
 */
export default function Nav(props) {
  console.log("props", props);
  const { name, age } = props;
  console.log("name", name);
  return (
    <div className="h-20 bg-green-600">
      Name: {name}
      <div>Age: {5 + age}</div>
    </div>
  );
}

Nav.propTypes = {
  name: Proptypes.string.isRequired,
  age: Proptypes.number.isRequired,
  data: Proptypes.arrayOf(
    Proptypes.shape({
      id: Proptypes.number.isRequired,
      title: Proptypes.string.isRequired,
    })
  ),
};

Nav.defaultProps = {
  name: "TestDefault",
  age: 12,
  data: [],
};
