import nextConnect from "next-connect";
import ErrorHandler from "@app/src/handler/error.handler";
import ProductController from "@app/src/controllers/product.controller";

const handler = nextConnect(ErrorHandler);
/**
 * DEFAULT dari next js
 * @param req
 * @param res
 */
handler
  .get(async (req, res) => {
    try {
      let { id } = req.query;
      console.log("idnya: " + id);

      const [err, data] = await new ProductController({
        key: "id",
        value: id,
      })._detail();

      if (err) {
        res.status(400);
        return res.json({
          error: true,
          status: 400,
          message: "produk ilang",
        });
      }

      if (!data) {
        res.status(404);
        return res.json({
          error: true,
          status: 404,
          message: "product ga ada",
        });
      }

      res.status(200);
      return res.json({
        error: false,
        status: 200,
        message: "Okey",
        data,
      });
    } catch (err) {
      res.status(500);
      return res.json({
        error: true,
        status: 500,
        message: "salahin server, dapetnya 500",
      });
    }
  })
  .delete(
    async (
        req,
        res
    )=> {
        try{
            const [ err, data ] =
            await new ProductController({
                key: "id",
                value: req.query?.id
            })._delete();

            if(err){
                res.status(400);
                return res.json({
                    error:true,
                    message: err?.message,
                })
            }

            if(!data){
                res.status(404);
                return res.json({
                    error: true,
                    message: "Product not found!"
                })
            }

            res.status(201);
            return res.json({});

        }catch(err){
            res.status(500)
            return res.json({
                error:true,
                message: err?.message
            })
        }

});

export default handler;
